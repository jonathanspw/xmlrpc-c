MAKEFILE_COMMON = $(HOME)/.fedora/common.mk
-include $(MAKEFILE_COMMON)

all:

SVN_VER ?= ${VERSION}

svn-sources:
	cd $(DESTDIR) . && svn export https://xmlrpc-c.svn.sourceforge.net/svnroot/xmlrpc-c/advanced@${SVN_REV} xmlrpc-c-${SVN_VER}
	cd $(DESTDIR) . && tar cJf xmlrpc-c-${SVN_VER}.tar.xz xmlrpc-c-${SVN_VER} --owner root --group root --mode=go-w,a+rX
